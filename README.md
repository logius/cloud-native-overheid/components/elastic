# Elastic Cloud for K8s (Elastic operator)

## Links

[release notes](https://www.elastic.co/guide/en/elasticsearch/reference/current/es-release-notes.html)

## Prerequisites

First, install the [elastic-operator](https://gitlab.com/logius/cloud-native-overheid/components/elastic-operator)

## Deploying

The Role deploys an Elastic cluster and Kibana using the k8s object definitions provided by the [quickstart](https://www.elastic.co/guide/en/cloud-on-k8s/current/k8s-quickstart.html)

## Index Lifecycle Policies.

A number of policies are added to elastic: _short_, _medium_, _long_, _default_, and _dotfiles_.

One general templates is added to elastic:

### default

The default selector of __[log-*]__ will match all indices created by the forwarders that are set up to prepend the indexname with __log__.

### Index lifecycle policies

To configure index templates that use a lifecycle policy, use the following config as an example:
```
config:
  elastic:
    index_templates:
      - name: abc-default-short
        index_patterns:
          - dev-*
          - test-*
          - prod-*
        ilp: short
```

## Keyhub

To be able to link the elastic component with OIDC There is an option to setup OIDC for elastic using Keyhub. Elastic is by default configured to use keycloak, the default configuration can be found in path __vars/main.yml__.

Configuration:
```
config:
  elastic:
    identity_provider: "keycloak"
```
To use Keyhub as IDP you have to set the following required settings in your cluster config:

* `identity_provider: "keyhub"`
* `config.system.identity_provider_url`

.Note: this will overwrite the default setting in __vars/main.yml__.

Setting of hostmachine:

Make sure the following env are set on the hostmachine to identify with keyhub:

* `KEYHUB_OIDC_ELASTIC_CLIENT_ID`
* `KEYHUB_OIDC_ELASTIC_CLIENT_SECRET`

OpenID Connect Scopes:

the following scopes are used by Elastic during authentication to authorize access to a user's details:

```yaml
rp.requested_scopes: openid profile
claims.principal: preferred_username
claims.groups: "groups"
```

## Elastic auditlogging

Enable elastic auditlogging with the following variables:
* `config.elastic.audit_logging_enabled: true`
* `config.kibana.audit_logging_enabled: true`

The following setup is an example how elastic (audit)logs can be tailed using a sidecar container.

```
config:
  elastic:
    additional_config:
      volumes:
        - name: elastic-audit
          emptyDir: {}
      volumemounts:
        - name: elastic-audit
          mountPath: /usr/share/elasticsearch/logs
    sidecars:
      - name: elastic-audit-logs
        image: busybox:latest
        args:
          - /bin/sh
          - -c
          - 'tail -n+1 -f /usr/share/elasticsearch/logs/elastic_audit.json'
        volumeMounts:
        - name: elastic-audit
          mountPath: /usr/share/elasticsearch/logs
```

Note for this example to work the `/usr/share/elasticsearch/config/log4j2.properties` file must be modified so elasticsearch stores the auditlogs in the `/usr/share/elasticsearch/logs/` directory.

## S3 repository plugin
An S3 repository plugin can be enabled to allow for backup snapshots to be made to S3, all following variables need to be configured when enabled.

Enable the plugin with the following variable
* `config.elastic.S3plugin.enabled: true`

An endpoint address has to be configured
* `config.elastic.S3plugin.endpoint`

The kubernetes secret name where the credentials are stored (required)
* `config.elastic.S3plugin.secret_name`

Credentials for S3 can be provided in two ways:
1. by using configuration items `config.elastic.S3Plugin.key` (access key) and `config.elastic.S3plugin.secret`
2. by providing the access key and secret key in a Kubernetes secret with name defined in `config.elastic.S3plugin.secret_name` (see below)

##### Add S3plugin secret to cluster
To avoid s3 secrets in cluster config, store the access key and secret key in a Kubernetes secret in the cluster instead.
1) Create secret in cluster and store access_key and secret_key

```bash
export NAMESPACE="{{ config.elastic.namespace }}"
export SECRET_NAME="{{ config.elastic.S3plugin.secret_name }}"
# Use plain text for ACCESS_KEY and SECRET_KEY, kubectl create secret will store values in base64.
export ACCESS_KEY="****"
export SECRET_KEY="****"

kubectl -n $NAMESPACE create secret generic $SECRET_NAME --from-literal=s3.client.default.access_key=$ACCESS_KEY --from-literal=s3.client.default.secret_key=$SECRET_KEY
```
2) In the cluster config only provide endpoint and secret_name:
```yaml
config:
  elastic:
    S3plugin:
      enabled: true
      endpoint: your-s3.endpoint.hostname
      secret_name: "name-of-secret-containing-credentials"
```
The following two settings for authentication to S3 and secret storage location in kubernetes
* `config.elastic.S3plugin.key`
* `config.elastic.S3plugin.secret`

## Elastic Snapshots
To use Elastic Snapshots set the `config.elastic.snapshot.enabled: true` and configure the necessary
parameters correctly.

Please check the docs for setting up Elastic snapshots:
- https://www.elastic.co/guide/en/elasticsearch/reference/current/snapshot-restore.html
- https://www.elastic.co/guide/en/elasticsearch/reference/current/getting-started-snapshot-lifecycle-management.html

Example:

```yaml
snapshot:
  enabled: true

  # Schedule for cleaning up elastic snapshot based on retention parameters set further below.
  # Default: "0 30 1 * * ?"
  retention_schedule: "0 45 1 * * ?"
  repositories:
    - name: elastic-snapshots

      # Name of s3 bucket for elastic snapshots.
      s3_bucket_name: elastic-snapshots

      # Subdirectory created within s3 bucket to store the snapshot. Can be empty.
      s3_base_path: ""

      # Parameters for SLM policy to create elastic snapshot
      policies:
        - schedule: '0 30 1 * * ?'
          lifecycle_policy_id: nightly-snapshots
          name: '<nightly-snap-{now/d}>'
          config:
            # (Optional, string or array of strings) Comma-separated list of data streams and indices to include in the snapshot.
            # Supports multi-index syntax. Defaults to an empty array ([]), which includes all data streams and indices, including
            # system indices. To exclude all data streams and indices, use -* or none.
            # for now disable to index all data
            #indices: []

            # Determines how wildcard patterns in the indices parameter match data streams and indices. Supports
            # comma-separated values, defaults to open,hidden. Other valid values all, closed, none
            #expand_wildcards: 'open,hidden'

            # (Optional, Boolean) If false, the snapshot fails if any data stream or index in indices is missing or closed. If true, the
            # snapshot ignores missing or closed data streams and indices. Defaults to false.
            ignore_unavailable: false

            # (Optional, Boolean) If true, include the cluster state in the snapshot. Defaults to true.
            include_global_state: true

            # Optional, Boolean) If false, the entire snapshot will fail if one or more indices included in the snapshot do not have all
            # primary shards available. Defaults to false. If true, allows taking a partial snapshot of indices with unavailable shards.
            partial: false

          # Snapshot retention configuration
          # Retention is always created.
          retention:
            # (Optional, time units) Time period after which a snapshot is considered expired and eligible for deletion. SLM deletes
            # expired snapshots based on the slm.retention_schedule.
            # default: "3d"
            expire_after: 3d

            # (Optional, integer) Minimum number of snapshots to retain, even if the snapshots have expired.
            # default: 3
            min_count: 3

            # (Optional, integer) Maximum number of snapshots to retain, even if the snapshots have not yet expired. If the number
            # of snapshots in the repository exceeds this limit, the policy retains the most recent snapshots and deletes older
            # snapshots. This limit only includes snapshots with a state of SUCCESS.
            # default: 5
            max_count: 5
```

## Crosss Cluster Replication

To use Elastic cross cluster replication set the `config.elastic.ccr.enabled: true` and configure the necessary
parameters correctly.

Please check the docs for setting up cross cluster replication:
- https://www.elastic.co/guide/en/elasticsearch/reference/current/remote-clusters.html
- https://www.elastic.co/guide/en/cloud-on-k8s/current/k8s-remote-clusters.html
- https://www.elastic.co/guide/en/elasticsearch/reference/current/remote-clusters-privileges.html

The code will set up the connection to the remote cluster. Setting up the follower patterns is done in the kibana-cli. 

Remote connections are configured with support for bi-directional replication. The replication user gets read and write privileges. 

Example:

```yaml
elastic:
  ccr:
    enabled: true
    # Name of remote cluster. This name will appear in Kibana to identify the remote connection.
    remote_cluster: staging
    # Name of the secret with the CA of the remote cluster. This secret needs to be provided. See instruction in elastic guide. 
    remote_secret: staging-ca
    # Address of transport service of the remote elastic cluster. 
    remote_address: elastic-transport.staging.example.com:9300
``` 

How to make the secret with the CA.
```
# Get CA from the leader cluster 
kubectl get secret elastic-es-transport-certs-public \
  -o go-template='{{index .data "ca.crt" | base64decode}}' > ca.crt

# Create secret with CA in the follower cluster. The name of the secret must be the same as provided in the variable `remote_secret`. 
kubectl create secret generic staging-ca --from-file=ca.crt
``` 